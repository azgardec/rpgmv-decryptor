#!/bin/bash

# Load Variables
source .gitlab/ci/variables/base.sh

get_package_id() {
    echo $(
        # Getting latest package id
        curl --header "PRIVATE-TOKEN: ${GITLAB_SECRET_API_KEY}" "https://gitlab.com/api/v4/projects/${CI_PROJECT_ID}/packages?sort=desc" | jq -c .[0].id
    )
}

get_package_files() {
    local package_id="$1"
    local json=$(
        curl --header "PRIVATE-TOKEN: ${GITLAB_SECRET_API_KEY}" "https://gitlab.com/api/v4/projects/${CI_PROJECT_ID}/packages/${package_id}/package_files" | jq -c .[-${#binaries_array[@]}:length]
    )
    local package_files=()

    for index in "${!binaries_array[@]}"; do
        package_files+=("$(echo "${json}" | jq -c .[${index}].id)")
    done

    echo ${package_files[@]}
}

get_download_url() {
    local file_id="$1"
    echo "https://gitlab.com/BlackYuzia/rpgmv-decryptor/-/package_files/${file_id}/download"
}

export -f get_package_id
export -f get_package_files
export -f get_download_url
