#!/bin/bash

get_upload_url() {
    local vt_api_url="$1"

    echo "$(curl --request GET --header "x-apikey: ${VT_SECRET_API_KEY}" "${vt_api_url}/files/upload_url" | jq ".data" | sed 's/\"//g')"
}

get_file_id() {
    local vt_upload_url="${1}" # use get_upload_url for get url
    local path_to_binary="${2}"

    echo "$(curl --request POST --header "x-apikey: ${VT_SECRET_API_KEY}" --form "file=@${path_to_binary}" "${vt_upload_url}" | jq ".data.id" | sed 's/\"//g')"
}

get_file_md5() {
    local vt_api_url="${1}"
    local binary_id="${2}"

    echo "$(curl --request GET --header "x-apikey: ${VT_SECRET_API_KEY}" "${vt_api_url}/analyses/${binary_id}" | jq ".meta.file_info.md5" | sed 's/\"//g')"
}

# get_upload_url  "$virustotal_api_url"
# get_file_id     "$virustotal_upload_url"  "$path_to_binary"
# get_file_md5    "$virustotal_api_url"     "$binary_id"

export -f get_upload_url
export -f get_file_id
export -f get_file_md5
