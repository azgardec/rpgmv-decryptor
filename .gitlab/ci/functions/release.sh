#!/bin/bash

generate_package_asset() {
    local asset_name="$1"
    local asset_url="$2"
    local asset_type="package"

    echo $(
        jq -n \
            --arg asset_name "$asset_name" \
            --arg asset_url "$asset_url" \
            --arg asset_type "$asset_type" \
            '{name: $asset_name, url: $asset_url, link_type: $asset_type }'
    )
}

generate_url_asset() {
    local asset_name="$1"
    local asset_url="$2"

    echo $(
        jq -n \
            --arg asset_name "$asset_name" \
            --arg asset_url "$asset_url" \
            '{name: $asset_name, url: $asset_url }'
    )
}

install_release_cli() {
    curl --location --output /usr/local/bin/release-cli "https://release-cli-downloads.s3.amazonaws.com/latest/release-cli-linux-amd64"
    chmod +x /usr/local/bin/release-cli
}

export -f generate_package_asset
export -f generate_url_asset
export -f install_release_cli
