import { mkdirSync } from "fs";

export const decryptStorage = "./decrypted";
export const encryptStorage = "./encrypted";

export function createDecryptStorage() {
    mkdirSync(decryptStorage, { recursive: true })
}

export function createEncryptStorage() {
    mkdirSync(encryptStorage, { recursive: true })
}