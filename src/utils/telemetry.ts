import { ofetch } from "ofetch";
import { ITelemetryData } from "../interfaces/telemetry";

export async function sendTelemetry(data: ITelemetryData) {
    try {
        await ofetch("https://api-rpgmvz.starserv.net/v2/telemetry/send", {
            method: "POST",
            body: data
        })
    } catch (error) {
        console.warn("Error on telemetry sending", error)
    }
}

export function getTelemetryEvent(name: string, telemetryData: ITelemetryData) {
    return telemetryData.events[name] || (telemetryData.events[name] = {
        startDate: performance.now(),
        endDate: 0,
        data: []
    })
}

export function getSystemUsage() {
    const memory = process.memoryUsage();
    const resource = process.resourceUsage();

    const filteredResource = {

    }

    for (const key of Object.keys(resource)) {
        if ([
            "userCPUTime",
            "systemCPUTime",
            "maxRSS",
            "fsRead",
            "fsWrite",
        ].includes(key)) {
            filteredResource[key] = resource[key]
        }
    }

    return {
        memory,
        resource: filteredResource // we don't need a lot of this data
    }
}

// This will be used only for display to user.
export let startDate = 0;
export let endDate = 0;

export function setStartTime() {
    startDate = performance.now()
}

export function setEndTime() {
    endDate = performance.now()
}