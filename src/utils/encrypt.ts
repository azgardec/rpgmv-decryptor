import { join, parse, relative } from "path";
import { RPGMVZ_Decrypted, RPGMVZ_Status, RPGMV_Encrypted, RPGMZ_Encrypted, RPGMakerEngine } from "../enums/rpgmvz";
import { IFile } from "../interfaces/decrypt";
import { gEvents } from "./events";
import { decryptStorage, encryptStorage } from "./storage";
import { mkdirSync, readFileSync, writeFileSync } from "fs";
import { buildFakeHeader, defaultHeaderLength, getStatus } from "./decrypt";
import { VerifyMode } from "../enums/start";

export async function encryptWithKey(key: string, files: IFile[]) {
    const encryptBuffer = Buffer.from(key, "hex")

    gEvents.emit("files.start", {
        name: "Encrypting...",
        stage: "encrypt",
        total: files.length
    })

    for (const file of files) {
        try {
            // Messy a bit...
            if (file.status === RPGMVZ_Status.Raw) {
                const { dir, name } = parse(file.path);
                const storagePath = relative(".", join(encryptStorage, dir, name + file.ext));
                const encryptedBody = new Uint8Array(readFileSync(file.path))

                for (let i = 0; i < defaultHeaderLength; i++) {
                    encryptedBody[i] = encryptedBody[i] ^ encryptBuffer[i] // decrypt xor
                }

                const encryptedFile = Buffer.concat([buildFakeHeader(), encryptedBody])
                // That's fine? 
                // I hope...
                file.header = Buffer.from(buildFakeHeader())

                mkdirSync(parse(storagePath).dir, { recursive: true }) // create the same folders struct inside decrypted folder
                writeFileSync(storagePath, encryptedFile)

                // Do this only after all manipulations with file
                file.status = RPGMVZ_Status.Encrypted
                gEvents.emit("files.new");
                continue;
            }
        } catch (error) {
            file.status = RPGMVZ_Status.Error
            console.error("Error on process with file", file, error)
        }
    }

    gEvents.emit("files.complete", {
        name: "Complete encrypting",
        stage: "encrypt",
        total: files.length
    })
}

export async function verifyFiles(files: IFile[], verifyMode: VerifyMode, engine: RPGMakerEngine) {
    gEvents.emit("files.start", {
        name: "Verification...",
        total: files.length,
        stage: "verify"
    })

    for (const file of files) {
        file.status = await getStatus(verifyMode, file)
        file.ext = getExtension(file.prevExt as any, engine)
        gEvents.emit("files.new")
    }

    gEvents.emit("files.complete", {
        name: "Complete verification",
        total: files.length,
        stage: "verify",
    })
}

/**
 * Required another function for encrypting mode
 *
 * @export
 * @param {RPGMVZ_Decrypted} ext
 * @return {*} 
 */
export function getExtension(ext: RPGMVZ_Decrypted, engine: RPGMakerEngine) {
    switch (ext) {
        case RPGMVZ_Decrypted.PNG:
            return engine === RPGMakerEngine.MV ? RPGMV_Encrypted.PNG : RPGMZ_Encrypted.PNG
        case RPGMVZ_Decrypted.M4A:
            return engine === RPGMakerEngine.MV ? RPGMV_Encrypted.M4A : RPGMZ_Encrypted.M4A
        case RPGMVZ_Decrypted.OGG:
            return engine === RPGMakerEngine.MV ? RPGMV_Encrypted.OGG : RPGMZ_Encrypted.OGG
        default:
            console.warn("Unknown extension %s, please report about it.", ext);
            return ext;
    }
}
