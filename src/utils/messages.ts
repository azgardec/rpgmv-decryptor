import { RPGMVZ_Status } from "../enums/rpgmvz";
import { IFile, ISystem } from "../interfaces/decrypt";
import { endDate, startDate } from "./telemetry";

/**
 * Just show message about key, title & did encrypted files are exist
 * 
 */
export function notifyAboutSystem(system?: ISystem) {
    if (!system) {
        return;
    }

    console.log(`Found '${system.encryptionKey}' key for '${system.gameTitle || 'No Title'}' game`);

    if (!system.hasEncryptedImages && !system.hasEncryptedAudio) {
        console.log("This game, suppose, don't have any encrypted assets!");
    }
    if (system.hasEncryptedAudio) {
        console.log("This game has encrypted audio")
    }
    if (system.hasEncryptedImages) {
        console.log("This game has encrypted images")
    }
}

export function displayResultPanel(files: IFile[], system?: ISystem,) {
    const decrypted = files.reduce((acc, cur) => cur.status === RPGMVZ_Status.Decrypted ? acc + 1 : acc, 0)
    const encrypted = decrypted + files.reduce((acc, cur) => cur.status === RPGMVZ_Status.Encrypted ? acc + 1 : acc, 0)
    const raw = files.reduce((acc, cur) => cur.status === RPGMVZ_Status.Raw ? acc + 1 : acc, 0)
    const unknown = files.reduce((acc, cur) => cur.status === RPGMVZ_Status.Unknown ? acc + 1 : acc, 0)
    const error = files.reduce((acc, cur) => cur.status === RPGMVZ_Status.Error ? acc + 1 : acc, 0)

    if (system) {
        console.log("Game Info:")
        console.table({
            title: system.gameTitle || "No Title in System.json",
            hasEncryptedAudio: system.hasEncryptedAudio,
            hasEncryptedImages: system.hasEncryptedImages,
            locale: system.locale,
            versionId: system.versionId,
            key: system.encryptionKey
        })
    }

    const stats = {
        total: files.length,
        "decrypted / encrypted ": `${decrypted} / ${encrypted}`, // in 99% cases this values are equal
        "total size": files.reduce((acc: any, cur) => acc + cur.size, 0n) / 100000n + " MB",
        "spend time": ((startDate - endDate) / 1000).toFixed(2) + "s"
    }
    // Remove 0 values from output
    if (raw) {
        (stats as any).filesWithoutEncryption = raw
    }
    if (unknown) {
        (stats as any).filesWithoutEncryption = unknown
    }
    if (error) {
        (stats as any).filesWithoutEncryption = error
    }
    console.log("Files stats:")
    console.table(stats)
}