import { gEvents } from "./events";

export function exit(message: string, code = 0, timeout = 30) {
    console.log(message)
    console.log(`I will close after ${timeout} seconds...`)
    // gEvents.emit("app.exit", {
    //     message,
    //     isError: code !== 0
    // })
    setTimeout(() => {
        process.exit(code);
    }, timeout * 1000)
}