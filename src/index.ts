import { StartMode } from "./enums/start";
import { attachTelemetryEvents, attachTimeSpendEvents } from "./utils/events";
import { promptStartup } from "./utils/menu";
import { startDecryptingAllFiles, startDecryptingImagesOnly, startEncryptingAllFiles } from "./utils/startup";
import { promptOnUpdate } from "./utils/version";

(async function () {
    attachTelemetryEvents()
    await promptOnUpdate()
    const { startup, extensions, verifyMode } = await promptStartup();
    if (!extensions || !verifyMode) {
        // User didn't choose something, we can't do anything.
        return;
    }
    attachTimeSpendEvents();
    switch (startup as StartMode) {
        case StartMode.AllFiles:
            return await startDecryptingAllFiles({ extensions, verifyMode });
        case StartMode.ImagesOnly:
            return await startDecryptingImagesOnly({ extensions, verifyMode });
        case StartMode.EncryptAll:
            return await startEncryptingAllFiles({ extensions, verifyMode })
    }
})()