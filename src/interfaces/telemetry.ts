import { StartMode } from "../enums/start";
import { ISystem } from "./decrypt";
export interface ITelemetryData {
    app: {
        version: string;
        startDate: number;
    }

    events: Record<string, ITelemetryEvent>;
    game: Omit<ISystem, "hasEncryptedImages" | "hasEncryptedAudio" | "encryptionKey"> & {
        hasImages: boolean;
        hasAudio: boolean;
        key: string;
    },
    system: {
        platform: string;
        arch: string;
        freemem: string;
        totalmem: string;
        type: string;
        version: string;
        cwd: string;
    }
}

export interface ITelemetryEvent {
    startDate: number;
    endDate: number;
    // replace by any should be easy way...
    data: {
        size?: any,
        name?: string,
        memory?: any,
        resource?: any,
        total?: number,
        update?: boolean,
        startup?: StartMode,
    }[]
}