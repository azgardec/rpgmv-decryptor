export interface IGitlabRelease {
    name: string
    tag_name: string
    description: string
    created_at: string
    released_at: string
    upcoming_release: boolean
    author: {
        id: number
        username: string
        name: string
        state: string
        avatar_url: string
        web_url: string
    },
    commit: {
        id: string
        short_id: string
        created_at: string
        parent_ids: string[]
        title: string;
        message: string;
        author_name: string;
        author_email: string;
        authored_date: string;
        committer_name: string;
        committer_email: string;
        committed_date: string;
        trailers: {},
        web_url: string;
    },
    commit_path: string;
    tag_path: string;
    assets: {
        count: 10,
        sources: {
            format: string;
            url: string;
        }[]
        links: {
            id: number,
            name: string
            url: string
            direct_asset_url: string
            link_type: "other" | "package"
        }[]
    },
    evidences: any[]
    _links: {
        closed_issues_url: string
        closed_merge_requests_url: string
        merged_merge_requests_url: string
        opened_issues_url: string
        opened_merge_requests_url: string
        self: string
    }
}