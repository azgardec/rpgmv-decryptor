export enum RPGMV_Encrypted {
    PNG = ".rpgmvp",
    M4A = ".rpgmvm",
    OGG = ".rpgmvo",
}

export enum RPGMZ_Encrypted {
    PNG = ".png_",
    M4A = ".m4a_",
    OGG = ".ogg_",
}

export enum RPGMVZ_Decrypted {
    PNG = ".png",
    M4A = ".m4a",
    OGG = ".ogg",
}

export enum RPGMVZ_System {
    Actors = "Actors.json",
    Animations = "Animations.json",
    Armors = "Armors.json",
    Classes = "Classes.json",
    CommonEvents = "CommonEvents.json",
    Enemies = "Enemies.json",
    Items = "Items.json",
    Skills = "Skills.json",
    States = "States.json",
    System = "System.json", // Currently we use only this one in both cases (lower & upper) 
    Tilesets = "Tilesets.json",
    Troops = "Troops.json",
    Weapons = "Weapons.json",
}

export enum RPGMVZ_Status {
    Unknown = 0,
    Encrypted = 1,
    Decrypted = 2,
    Raw = 3,
    Error = 4
}

export enum RPGMakerEngine {
    MV = 0,
    MZ = 1
}