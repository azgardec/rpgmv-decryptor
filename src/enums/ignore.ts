export const ignoreFolders = [
    "!node_modules",
    "!.git",
    "!.vscode",
    "!decrypted",
    "!encrypted",
]