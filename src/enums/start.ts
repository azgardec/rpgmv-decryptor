// todo: create steps based startup instead
// 1. Mode? (key or without key)
// 2. Select system (if mode with key is selected)
// 3. Files Extensions (pre defined sets or set manualy)
// 4. Set manualy extensions (if selected in 3-th step)
// 5. Set depth for scan
// 5. ...
// 6. Display results
export enum StartMode {
    AllFiles = 1,
    ImagesOnly = 2,
    EncryptAll = 3
}

export enum VerifyMode {
    ByHeader = 1,
    ByExtension = 2
}